import React, { Component } from "react"
import {  Carousel} from 'react-bootstrap';

class CarouselComp extends Component{

render(){
    return (
<div>        
<div className='container-fluid'>
<div className="row">
<div className="col-0">
</div>
<div className="col-12">
<Carousel >
  <Carousel.Item style={{'height':"300px"}}>
    <img
      style={{'height':"300px"}}
      className="d-block w-100"
      src="../Slide1.JPG"
      alt="First slide"
    />
    <Carousel.Caption>
      
      <p>Swap your items with others.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item style={{'height':"300px"}}>
    <img
      style={{'height':"300px"}}
      className="d-block w-100"
      src="Slide2.JPG"
      alt="secound slide"
    />

    <Carousel.Caption>
      
      <p>Find a Swapper</p>
    </Carousel.Caption>
  </Carousel.Item>
  
</Carousel>
</div>
</div>
</div>
</div>
    );
}

}
export default CarouselComp