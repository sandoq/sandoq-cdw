import React, { Component } from 'react'
import {  Navbar, Form, FormControl } from 'react-bootstrap';
class NavComp extends Component{

    render(){
        return (
            <div>
           <Navbar bg="light" variant="light">
                <Navbar.Brand href="#home">
                    <img
                    src="../logo.png"
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt="React Bootstrap logo"
                />{' '}
                   SwapGeeBee
                </Navbar.Brand>
          
          
           </Navbar>
          </div> 
        );
    }


}
export default NavComp