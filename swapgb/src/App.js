import React from 'react';
import NavComp from './Components/NavComp';
import CarouselComp from './Components/CarouselComp'

import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <div >
      
      <NavComp/>
      
      <CarouselComp/>
    </div>
  );
}

export default App;
